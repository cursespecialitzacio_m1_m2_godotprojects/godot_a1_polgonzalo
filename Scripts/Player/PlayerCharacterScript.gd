extends Sprite2D

const VELOCITY_CHARACTER = 700
const MOVE_LEFT = "move_left"
const MOVE_RIGHT = "move_right"
const MOVE_UP = "move_up"
const MOVE_DOWN = "move_down"
# Called when the node enters the scene tree for the first time.
func _ready():
	pass
#
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	movement(delta)
	
func movement(delta) :
	var vel = Vector2(0,0)
	if( Input.is_action_pressed(MOVE_LEFT) ) :
		vel.x += -1
	if ( Input.is_action_pressed(MOVE_RIGHT) ) :
		vel.x += 1	
	if( Input.is_action_pressed(MOVE_UP) ) :
		vel.y += 1 
	if ( Input.is_action_pressed(MOVE_DOWN) ) :
		vel.y += -1	
	move_local_x(vel.x * VELOCITY_CHARACTER * delta)
	move_local_y(vel.y * VELOCITY_CHARACTER * delta)
